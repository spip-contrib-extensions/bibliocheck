<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

/* pour que le pipeline ne rale pas ! */
function bibliocheck_autoriser(){}

/**
 * Autorisation de vérifier les références biblio
  * 
 * @param string $faire : l'action à faire
 * @param string $type : le type d'objet sur lequel porte l'action
 * @param int $id : l'identifiant numérique de l'objet
 * @param array $qui : les éléments de session de l'utilisateur en cours
 * @param array $opt : les options
 * @return boolean true/false : true si autorisé, false sinon
 */
function autoriser_biblio_verifier_dist($faire, $type, $id, $qui, $opt){
	$autorise = false;

	include_spip('inc/config');
	$type = lire_config('bibliocheck/autorisation_type');
	if($type){
		switch($type) {
			case 'webmestre':
				// Webmestres uniquement
				$autorise = ($qui['webmestre'] == 'oui');
				break;
			case 'par_statut':
				// Traitement spécifique pour la valeur 'tous'
				if(in_array('tous',lire_config('bibliocheck/autorisation_statuts',array('0minirezo')))){
					return true;
				}
				// Autorisation par statut
				$autorise = in_array($qui['statut'], lire_config('bibliocheck/autorisation_statuts',array()));
				break;
			case 'par_auteur':
				// Autorisation par id d'auteurs
				$autorise = in_array($qui['id_auteur'], lire_config('bibliocheck/autorisation_auteurs',array()));
				break;
		}
		if($autorise == true){
			return $autorise;
		}
		$utiliser_defaut = false;
	}

	// Si pas configuré ou pas autorisé dans la conf => webmaster
	$autorise = ($qui['webmestre'] == 'oui');

	return $autorise;
}

