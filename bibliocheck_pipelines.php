<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function bibliocheck_affiche_milieu($flux) {
	$exec = $flux['args']['exec'];
	// page config zotspip
	if ($exec=='configurer_zotspip' && autoriser('webmestre')) {
		$flux['data'] .= recuperer_fond('prive/inclure/configurer_bibliocheck');
	}
	// si on est sur la page ticket
	if ($exec=='ticket'){
		$texte = recuperer_fond(
				'prive/inclure/ticket_complement_bibliocheck',
				array(
					'id_ticket'=>$flux['args']['id_ticket']
				)
		);
		if ($p=strpos($flux['data'],"<!--affiche_milieu-->"))
			$flux['data'] = substr_replace($flux['data'],$texte,$p,0);
		else
			$flux['data'] .= $texte;
	}
	return $flux;
}

function bibliocheck_affiche_droite($flux) {
	$exec = $flux['args']['exec'];
	if ($exec=='ticket')
		$flux['data'] .= recuperer_fond('prive/inclure/maj_zotspip');
	return $flux;
}

/**
 * Inserer la CSS de le bibliocheck
 *
 * @param $flux
 * @return mixed
 */
function bibliocheck_insert_head_css($flux) {
	$flux .= '<link rel="stylesheet" type="text/css" href="'.find_in_path('css/bibliocheck.css').'" />';
	return $flux;
}
